<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'leading');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', 'root');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'p%h(--;GLTI)*>2e$N$e$[7w1Vf]X!c>XQ-MdNfVyPv@&)>U?d-@7qe?_-4MPYu}');
define('SECURE_AUTH_KEY', 'xyiw+C`]ZP:09$N7|Ul;S|{&_`Me`FFo2c>snWn4duxE.kx:AkSS-&k2_UY7P49)');
define('LOGGED_IN_KEY', 'eKAE-l17*&bWeU_%!y,e8j~FLcgounTfAQzp^72y{ES%){BEyxg3diV&.N<0xC51');
define('NONCE_KEY', 'q(l@oe(OA;@=(#[%1V~{ ?PvLa8m9bsapFjT]&Ddyk:|+&d +#]ZVz`CpAPQbr5H');
define('AUTH_SALT', 'iqAh#G ^01W%RC^uIB`r*h<e@8O~Q-npz^w+%qM+bVYcY,|C_]b;MIE(d ^_bg*R');
define('SECURE_AUTH_SALT', '$AEceDuEOeW)T_254!TL=FE/vS}RDGZAept=:&kPRq,)O@#N55NVztrzfpip&?LB');
define('LOGGED_IN_SALT', '|Zor`]_9hnS(Abq:c;*2CI>Grn^s2mr-cOZ xK0%oJ]LjhHy H=w=7#pUhEq,$qA');
define('NONCE_SALT', '|~Zm(#!:[DcE@aP^}/:u#|)uMzlun2X_8)PZz}fs0H5`-~6Uzhs6(+N-Pvnx(qte');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

